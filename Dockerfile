FROM docker:latest
WORKDIR /app

RUN apk add --update curl bash gnupg ca-certificates nodejs npm git \
  && curl https://cli-assets.heroku.com/install.sh | sh \
  && rm -rf /var/cache/apk/*

CMD ["heroku"]